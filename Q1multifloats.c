#include <stdio.h>
#include <stdlib.h>

int main()
{
    float x , y, product;
    printf("Please enter two float numbers\n");
    scanf("%f", &x);
    scanf("%f", &y);
    product = x * y;
    printf("%.2f * %.2f = %.4f\n", x, y, product);

    return 0;
}
